#include <pthread.h>

#define FIRST_NB 2
#define DEF_THREADS_NB 8
#define MAX_WITHOUT_MULTITHREADING 100000

struct chainedNumber {
    long long value;
    struct chainedNumber *next;
    struct chainedNumber *prev;
};

struct protectedLongLong {
    long long value;
    pthread_mutex_t mutex;
};

struct protectedChainedNumber {
    struct chainedNumber *value;
    pthread_mutex_t mutex;
};

// Convert a char array into an integer
long long toInt(char text[]);

void* addPrimeNumbers(void *param);