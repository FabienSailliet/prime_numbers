#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <time.h>

#ifdef _WIN32
    #include <windows.h>
#else
    #include <unistd.h>
#endif

#include "primenumbers.h"

// Prime numbers chain pointers
struct chainedNumber first = {.value=FIRST_NB, .next=NULL, .prev=NULL};
struct protectedChainedNumber last = {.value=&first, .mutex=PTHREAD_MUTEX_INITIALIZER};

// Last tested value
struct protectedLongLong testedNumber;

int main(int argc, char *argv[]) {
    
    long long max, min;
    int threads_nb;

    // Get the max number from arguments
    if (argc == 3 || argc == 4) {
        min = toInt(argv[1]);
        max = toInt(argv[2]);

        threads_nb = (argc >= 4)? toInt(argv[3]) : DEF_THREADS_NB;
    }
    else {
        printf("Usages :\n"
               "    primenumbers MIN MAX\n"
               "    primenumbers MIN MAX THREADS_NUMBER\n");
        return 1;
    }

    time_t start, end;
    
    testedNumber.value = 1;
    testedNumber.mutex = (pthread_mutex_t) PTHREAD_MUTEX_INITIALIZER;

    printf("Calculating...\n");

    start = time(NULL);

    long long firstMax = (max <= MAX_WITHOUT_MULTITHREADING)? max : MAX_WITHOUT_MULTITHREADING;

    pthread_t threads[threads_nb];
    pthread_create(&threads[0], NULL, addPrimeNumbers, &firstMax);
    pthread_join(threads[0], NULL);

    for (int i=0 ; i<threads_nb ; i++) {
        pthread_create(&threads[i], NULL, addPrimeNumbers, &max);
    }

    for (int i=0 ; i<threads_nb ; i++) {
        pthread_join(threads[i], NULL);
    }

    end = time(NULL);

    printf("Calculating done!\n");

    struct chainedNumber *curr = &first;
    while (curr != NULL) {
        if (curr->value >= min) {
            printf("%lld\n", curr->value);
        }

        curr = curr->next;
    }

    printf("Done in %ld seconds\n", end - start);
    
}

void* addPrimeNumbers(void *param) {
    const long long max = *(const long long*) param;
    
    // While max number is not reached
    while (1) {

        /*---------- Read the number to test ----------*/
        pthread_mutex_lock(&testedNumber.mutex);

        // Break if reached the max number
        if (testedNumber.value >= max) {
            pthread_mutex_unlock(&testedNumber.mutex);
            break;
        }

        register long long toTest = testedNumber.value + 2;

        testedNumber.value = toTest;

        pthread_mutex_unlock(&testedNumber.mutex);

        /*---------------------------------------------*/

        register struct chainedNumber *curr = &first;
        curr = &first;

        bool prime = true;
        register long double sqrt = sqrtl(toTest);

        // Check if tested number is prime
        while (curr != NULL && curr->value <= sqrt) {

            // If find a divider
            if (toTest % curr->value == 0) {
                prime = false;
                break;
            }

            curr = curr->next;  
        }

        if (prime) {

            // Create the new number
            struct chainedNumber *new = malloc(sizeof(struct chainedNumber));
            new->value = toTest;

            /*---------- Add the number at the end of the chain ----------*/

            pthread_mutex_lock(&last.mutex);

            // Search where insert the new number
            struct chainedNumber *previous = last.value;
            while (previous->prev != NULL && toTest < previous->value) {
                previous = previous->prev;
            }

            // Save the next number
            struct chainedNumber *next = previous->next;

            // Fill and chain the new one
            new->next = next;
            new->prev = previous;

            // Chain its neightboors
            previous->next = new;

            // Chain to the next if there is one
            if (new->next != NULL) {
                new->next->prev = new;
            }
            // Else, change the head
            else {
                last.value = new;
            }

            pthread_mutex_unlock(&last.mutex);

            /*------------------------------------------------------------*/
        }        
    }

    return NULL;
}

// Convert a char array into an integer
long long toInt(char text[]) {
    long long ret = 0;
    for (int i=0 ; text[i]!='\0' ; i++) {
        ret = ret*10 + text[i]-'0';
    }

    return ret;
}