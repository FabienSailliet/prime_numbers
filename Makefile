CC=gcc
CFLAGS=-Wall -lm -pthread
EXECS=primenumbers

all: $(EXECS)

%: %.c %.h
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f *.o

mrproper:
	rm -f $(EXECS)

run_%: %
	./$<
